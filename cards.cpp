#include "cards.h"
#include <cstdlib>
#include <iostream>

using namespace std;

/* *************************************************
   Card Class Implementations
   ************************************************* */

   /*
      Default constructor for the Card class.
      It could give repeated cards. This is OK.
      Most variations of Blackjack are played with
      several decks of cards at the same time.
   */
Card::Card() {
    int r = 1 + rand() % 4;
    switch (r) {
    case 1: suit = OROS; break;
    case 2: suit = COPAS; break;
    case 3: suit = ESPADAS; break;
    case 4: suit = BASTOS; break;
    default: break;
    }

    r = 1 + rand() % 10;
    switch (r) {
    case  1: rank = AS; value = 1; break;
    case  2: rank = DOS; value = 2; break;
    case  3: rank = TRES; value = 3; break;
    case  4: rank = CUATRO; value = 4; break;
    case  5: rank = CINCO; value = 5; break;
    case  6: rank = SEIS; value = 6; break;
    case  7: rank = SIETE; value = 7; break;
    case  8: rank = SOTA; value = .5; break;
    case  9: rank = CABALLO; value = .5; break;
    case 10: rank = REY; value = .5; break;
    default: break;
    }
}

// Accessor: returns a string with the suit of the card in Spanish 
string Card::get_spanish_suit() const {
    string suitName;
    switch (suit) {
    case OROS:
        suitName = "oros";
        break;
    case COPAS:
        suitName = "copas";
        break;
    case ESPADAS:
        suitName = "espadas";
        break;
    case BASTOS:
        suitName = "bastos";
        break;
    default: break;
    }
    return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish 
string Card::get_spanish_rank() const {
    string rankName;
    switch (rank) {
    case AS:
        rankName = "As";
        break;
    case DOS:
        rankName = "Dos";
        break;
    case TRES:
        rankName = "Tres";
        break;
    case CUATRO:
        rankName = "Cuatro";
        break;
    case CINCO:
        rankName = "Cinco";
        break;
    case SEIS:
        rankName = "Seis";
        break;
    case SIETE:
        rankName = "Siete";
        break;
    case SOTA:
        rankName = "Sota";
        break;
    case CABALLO:
        rankName = "Caballo";
        break;
    case REY:
        rankName = "Rey";
        break;
    default: break;
    }
    return rankName;
}

// Accessor: returns a string with the suit of the card in English 
string Card::get_english_suit() const {
    string suitName;
    switch (suit) {
    case OROS:
        suitName = "Coins";
        break;
    case COPAS:
        suitName = "Cups";
        break;
    case ESPADAS:
        suitName = "Swords";
        break;
    case BASTOS:
        suitName = "Clubs";
        break;
    default: break;
    }
    return suitName;
}

// Accessor: returns a string with the rank of the card in English 
string Card::get_english_rank() const {
    string rankName;
    switch (rank) {
    case AS:
        rankName = "Ace";
        break;
    case DOS:
        rankName = "Two";
        break;
    case TRES:
        rankName = "Three";
        break;
    case CUATRO:
        rankName = "Four";
        break;
    case CINCO:
        rankName = "Five";
        break;
    case SEIS:
        rankName = "Six";
        break;
    case SIETE:
        rankName = "Seven";
        break;
    case SOTA:
        rankName = "Jack";
        break;
    case CABALLO:
        rankName = "Knight";
        break;
    case REY:
        rankName = "King";
        break;
    default: break;
    }
    return rankName;
}

// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
    return static_cast<int>(rank) + 1;
}

//Returns card's numerical value
double Card::get_value() const {
    return value;
}


// Comparison operator for cards
// Returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
    return rank < card2.rank;
}

//Friends, Overloaded operator<< 
ostream& operator<< ( ostream& out, const Card* card) {
    out << "\t" << (*card).get_spanish_rank() << " de " << (*card).get_spanish_suit()
        << "\t (" << (*card).get_english_rank() << " of " << (*card).get_english_suit() << ") \n";
    return out;
}

fstream& operator<< ( fstream& out, const Card* card) {
    out << "\t" << (*card).get_spanish_rank() << " de " << (*card).get_spanish_suit()
        << "\t (" << (*card).get_english_rank() << " of " << (*card).get_english_suit() << ") \n";
    return out;
}


/* *************************************************
   Hand Class Implementations
 ************************************************* */
//Constructors
Hand::Hand() : total(0), size(0) { }

Hand::Hand(double m) : total(m), size(0) { }

//Accessors: total score w/in round
double Hand::get_total() const {
    return total;
}

//Adds new card
void Hand::add_a_card() {
    Card* z = new Card;
    cout << "\nNew Card: \n" << z << "\n";
    cards.push_back(z); 
    total += (*z).get_value();
    size++;
}

//Clears cards after round ends
void Hand::end_round() {
    total = 0;
    for (auto i = 0; i < cards.size(); i++) {
        delete (cards[i]);
    }
    cards.clear();
    size = 0;
}

//Friends, Overloaded operator<< 
ostream& operator<<(ostream& out, const Hand& hand) {
    for (auto i = 0; i < hand.size; i++) {
        out << hand.cards[i];
    }

    return out;
}

fstream& operator<<( fstream& out, const Hand& hand) {
    for (auto i = 0; i < hand.size; i++) {
        out << hand.cards[i];
    }
    
    return out; 
}


/* *************************************************
    Player Class Implementations
************************************************* */
// Constructor -  Assigns initial amount of money
Player::Player(int m) : money(m) {}

//Accessor: returns current money
int Player::get_money() const {
    return money;
}

//Setter: adds or subtracts to money based on results
void Player::bet_resutls(double bet) {
    money += bet;
}