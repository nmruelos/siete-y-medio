#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"

using namespace std;

int main() {

    ofstream gamelog;
    gamelog.open("gamelog.txt");

    Player dealer(900);
    Hand dealer_hand(0);

    Player A(100);
    Hand A_hand(0);
    int bet;
    char cont;

    int game_number = 1;

    //Loop for each round
    while (dealer.get_money() > 0 && A.get_money() > 0){

        cout << "You have " << A.get_money() << "\t Enter bet: ";
        cin >> bet;

        gamelog << "----------------------------------------------- \n "
            << "\nGame Number: " << game_number << "\t \t Money Left: " << A.get_money()
            << "\nBet: " << bet;;

        cont = 'y';

        //Player's Turn
        while (cont == 'y') {
            A_hand.add_a_card();
            cout << "Your cards: \n";
            cout << A_hand;
            cout << "Your total is " << A_hand.get_total()
                    << ". Do you want another card (y/n)? ";
            cin >> cont;
        }
        
        //Dealer's Turn
        while ( dealer_hand.get_total() < 5.5 ){ 
            dealer_hand.add_a_card();
            cout << "Dealer's cards: \n";
            cout << dealer_hand;
            cout << "The dealer's total is " << dealer_hand.get_total() << ". \n";
        }

        //Determine winner of round
        if (A_hand.get_total() == dealer_hand.get_total()) {
            cout << "\nNobody wins! \n\n";
        }else if(A_hand.get_total() > 7.5 || (A_hand.get_total() < dealer_hand.get_total() && dealer_hand.get_total() < 7.5) ){
            cout << "\nYou lose! \n\n";
            A.bet_resutls(-bet);
            dealer.bet_resutls(bet);
        }
        else {
            cout << "\nYou win " << bet << "! \n\n";
            A.bet_resutls(bet);
            dealer.bet_resutls(-bet);
        }


        //Output results into file
        gamelog << "\nYour cards: \n" << A_hand
            << "\nYour total: " << A_hand.get_total();

        gamelog << "\n\nDealer's cards: \n" << dealer_hand
            << "\nDealer's total: " << dealer_hand.get_total() << "\n";

        //Cleaning memory
        dealer_hand.end_round();
        A_hand.end_round();
        game_number++;

    }

    gamelog.close();

    return 0;
}